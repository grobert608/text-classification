name := "text-classification"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.1.0"

// https://mvnrepository.com/artifact/org.specs2/specs2-scalacheck
libraryDependencies += "org.specs2" %% "specs2-scalacheck" % "4.8.3" % Test

libraryDependencies ++= Seq("org.specs2" %% "specs2-core" % "4.8.3" % "test")

scalacOptions in Test ++= Seq("-Yrangepos")

// https://mvnrepository.com/artifact/com.opencsv/opencsv
libraryDependencies += "com.opencsv" % "opencsv" % "5.1"

// https://mvnrepository.com/artifact/org.openjfx/javafx
libraryDependencies += "org.openjfx" % "javafx" % "13.0.2" pomOnly()

// https://mvnrepository.com/artifact/org.fxmisc.richtext/richtextfx
libraryDependencies += "org.fxmisc.richtext" % "richtextfx" % "0.10.3"

// https://mvnrepository.com/artifact/com.typesafe.play/play-json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.8.1"

// https://mvnrepository.com/artifact/org.apache.lucene/lucene-analyzers-common
libraryDependencies += "org.apache.lucene" % "lucene-analyzers-common" % "8.4.1"

libraryDependencies += "com.typesafe.play" %% "play-ahc-ws-standalone" % "2.1.2"
libraryDependencies += "com.typesafe.play" %% "play-ws-standalone-json" % "2.1.2"

// https://mvnrepository.com/artifact/com.github.pureconfig/pureconfig
libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.12.3"

// https://mvnrepository.com/artifact/org.slf4j/slf4j-simple
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.30" % Test

libraryDependencies += "org.apache.commons" % "commons-text" % "1.8"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

libraryDependencies += "com.typesafe.akka" %% "akka-persistence-typed" % "2.6.4"

libraryDependencies += "com.typesafe.akka" %% "akka-serialization-jackson" % "2.6.4"

libraryDependencies += "com.typesafe.akka" %% "akka-persistence-query" % "2.6.4"

libraryDependencies += "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"

libraryDependencies += guice

enablePlugins(PlayScala)
disablePlugins(PlayLayoutPlugin)

PlayKeys.playMonitoredFiles ++=
  (sourceDirectories in
    (Compile, TwirlKeys.compileTemplates)).value