package classifier

import org.specs2.mutable.Specification
import textUtils.TextModifier

class ClassifierSpec extends Specification {

  "Classifier" should {
    "be able to guess classes" in {
      val c = NaiveBayesLearningAlgorithm()
      c.addExample(TextModifier.cleanAndSplit("предоставляю услуги разработчика"), "SPAM")
      c.addExample(TextModifier.cleanAndSplit("спешите купить акции"), "SPAM")
      c.addExample(TextModifier.cleanAndSplit("надо купить молоко"), "HAM")

      val bestClass = c.classifier.classify(TextModifier.cleanAndConvert("надо купить сигареты"), 0)
      bestClass.category must_== ("neutral")
    }
  }
}



