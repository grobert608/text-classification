package classifier

import org.specs2.mutable.Specification
import play.api.libs.json.{JsError, JsSuccess, Json}
import vk.units.UsersResponse

class UserFromJSONSpec extends Specification {
  "User" should {
    "be correct" in {
      val json = Json.parse(
        """
          |{
          |"response": [{
          |"id": 210700286,
          |"first_name": "Lindsey",
          |"last_name": "Stirling",
          |"is_closed": false,
          |"can_access_closed": true,
          |"city": {
          |"id": 5331,
          |"title": "Los Angeles"
          |},
          |"photo_100": "https://sun9-59.u...5FuH5Kb-o.jpg?ava=1",
          |"verified": 1
          |}]
          |}
          |""".stripMargin)

      val user: String = json.validate[UsersResponse] match {
        case JsSuccess(users, _) => users.toString
        case e: JsError => JsError.toJson(e).toString
      }

      user must_== ("List(Users(210700286,Lindsey,Stirling,https://sun9-59.u...5FuH5Kb-o.jpg?ava=1))")
    }
  }
}