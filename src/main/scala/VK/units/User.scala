package vk.units

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class User(id: Long, firstName: String, lastName: String, photo100: String)

object User {
  implicit val userReads: Reads[User] = (
    (__ \ "id").read[Long] and
      (__ \ "first_name").read[String] and
      (__ \ "last_name").read[String] and
      (__ \ "photo_100").read[String]) (User.apply _)
}

case class UsersResponse(users: List[User])

object UsersResponse {
  implicit def usersReads: Reads[UsersResponse] = (__ \ "response").read[List[User]](Reads.list(User.userReads)).map(UsersResponse(_))
}