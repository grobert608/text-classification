package vk.units

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class Group(id: Long, name: String, photo_100: String)

object Group {
  implicit val groupReads: Reads[Group] = (
    (__ \ "id").read[Long] and
      (__ \ "name").read[String] and
      (__ \ "photo_100").read[String]) (Group.apply _)
}

case class GroupsResponse(groups: List[Group])

object GroupsResponse {
  implicit def usersReads: Reads[GroupsResponse] = (__ \ "response").read[List[Group]](Reads.list(Group.groupReads)).map(GroupsResponse(_))
}