package server

import akka.actor.ActorSystem
import play.api.libs.json.{JsError, JsSuccess, JsValue}
import play.api.libs.ws.JsonBodyReadables._
import play.api.libs.ws.ahc.StandaloneAhcWSClient
import vk.units._

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future

class VKserver() {
  implicit val system: ActorSystem = ActorSystem()

  val token = Configuration().loadToken.accessToken

  val wsClient = StandaloneAhcWSClient()

  def getNews: Future[Either[String, List[NewsFeed]]] = wsClient.url(s"https://api.vk.com/method/newsfeed.get?v=5.52&filters=post&access_token=$token").get().map { response =>
    val json = response.body[JsValue]
    json.validate[NewsFeedsResponse] match {
      case JsSuccess(newsFeedsResponse, _) => Right(newsFeedsResponse.newsFeed)
      case e: JsError => Left(JsError.toJson(e).toString())
    }
  }

  def getUser(id: Long): Future[Either[String, List[User]]] = wsClient.url(s"https://api.vk.com/method/users.get?v=5.52&user_ids=$id&fields=photo_100&access_token=$token").get().map { response =>
    val json = response.body[JsValue]
    json.validate[UsersResponse] match {
      case JsSuccess(usersResponse, _) => Right(usersResponse.users)
      case e: JsError => Left(JsError.toJson(e).toString())
    }
  }

  def getGroups(id: Long): Future[Either[String, List[Group]]] = wsClient.url(s"https://api.vk.com/method/groups.getById?v=5.52&group_ids=$id&access_token=$token").get().map { response =>
    val json = response.body[JsValue]
    json.validate[GroupsResponse] match {
      case JsSuccess(groupsResponse, _) => Right(groupsResponse.groups)
      case e: JsError => Left(JsError.toJson(e).toString())
    }
  }
}
