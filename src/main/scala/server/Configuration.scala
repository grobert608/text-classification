package server

import pureconfig.generic.auto._
import server.Configuration.VKConfig

case class Configuration() {
  def loadToken: VKConfig = pureconfig.ConfigSource.file("src/main/resources/vk.conf").loadOrThrow[VKConfig]
}

object Configuration {

  case class VKConfig(accessToken: String)

}