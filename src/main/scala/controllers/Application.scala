package controllers

import java.nio.file.Paths

import actors.NewsFeedActor
import akka.NotUsed
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.adapter._
import akka.persistence.query.journal.leveldb.scaladsl.LeveldbReadJournal
import akka.persistence.query.{EventEnvelope, PersistenceQuery}
import akka.stream.scaladsl.Source
import akka.util.Timeout
import classifier.NaiveBayesLearningAlgorithm
import javax.inject.{Inject, Singleton}
import play.api.mvc._
import server.VKserver
import textUtils.{ReadDataFromCSV, TextMarking, TextModifier}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class Application @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  val positiveExs: List[NaiveBayesLearningAlgorithm.DataSet] = ReadDataFromCSV.getExamples(Paths.get("src/main/resources/positive.csv")).map(res => NaiveBayesLearningAlgorithm.DataSet(TextModifier.cleanAndSplit(res), "positive"))
  val negativeExs: List[NaiveBayesLearningAlgorithm.DataSet] = ReadDataFromCSV.getExamples(Paths.get("src/main/resources/negative.csv")).map(res => NaiveBayesLearningAlgorithm.DataSet(TextModifier.cleanAndSplit(res), "negative"))

  val server = new VKserver()

  implicit val actorSystem = server.system.toTyped

  implicit val timeout: Timeout = 10.minutes

  implicit val ec: ExecutionContext = actorSystem.executionContext

  val bla = NaiveBayesLearningAlgorithm()
  bla.addExamples(positiveExs)
  bla.addExamples(negativeExs)

  def showForm() = Action {
    Ok(views.html.form(None, None, None))
  }

  def classifyText(text: String) = Action {
    if (text.nonEmpty) {
      val str = TextModifier.shielding(text)
      val bestClass = bla.classifier.classify(TextModifier.cleanAndConvert(str), 3)
      Ok(views.html.result(TextMarking.markingTermsHTML(str, bestClass.terms), bestClass.category))
    }
    else {
      Ok(views.html.form(None, None, None))
    }
  }

  val typedActor: ActorRef[NewsFeedActor.CommandProtocol] = actorSystem.systemActorOf(NewsFeedActor.behavior(bla), "newsFeed")

  val queries: LeveldbReadJournal = PersistenceQuery(actorSystem).readJournalFor[LeveldbReadJournal](LeveldbReadJournal.Identifier)

  val events: Source[EventEnvelope, NotUsed] = queries.currentEventsByPersistenceId("myNewsFeed", fromSequenceNr = 0)

  def viewNewsActor() = Action {

    val result: Future[List[NewsFeedActor.News]] = typedActor ? NewsFeedActor.Get

    Ok(views.html.newsfeed(Await.result(result, 10.minutes)))
  }
}