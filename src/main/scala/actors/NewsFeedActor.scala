package actors

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, RetentionCriteria}
import classifier.NaiveBayesLearningAlgorithm
import server.VKserver
import textUtils.{TextMarking, TextModifier}
import vk.units.{NewsFeed, User}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

trait MySerializable

object NewsFeedActor {

  case class SourceOfNews(name: String, photo: String) extends MySerializable

  case class News(text: String, classification: String, source: Option[SourceOfNews]) extends MySerializable

  sealed trait CommandProtocol

  case class Get(replyTo: ActorRef[List[News]]) extends CommandProtocol

  case class UpdateData(value: List[News]) extends CommandProtocol

  sealed trait EventProtocol extends MySerializable

  case class Refresh(value: List[News]) extends EventProtocol

  val server = new VKserver()

  def commandHandler(bla: NaiveBayesLearningAlgorithm, ctx: ActorContext[NewsFeedActor.CommandProtocol]): (List[News], CommandProtocol) => Effect[EventProtocol, List[News]] = {
    (value, command) =>
      command match {
        case Get(replyTo) =>
          Effect.reply(replyTo)(value)
        case UpdateData(value) =>
          ctx.pipeToSelf(getNews(bla)) {
            case Success(list) => UpdateData(list)
            case Failure(_) => UpdateData(List.empty)
          }
          if (value.nonEmpty) {
            Effect.persist(Refresh(value))
          } else {
            Effect.none
          }
      }
  }

  val eventHandler: (List[News], EventProtocol) => List[News] = {
    (state, event) =>
      event match {
        case Refresh(valueNew) => {
          if (valueNew.nonEmpty) {
            (valueNew ++ state).take(100).distinct
          } else {
            state
          }
        }
      }
  }

  def behavior(bla: NaiveBayesLearningAlgorithm): Behavior[CommandProtocol] =
    Behaviors.withTimers { timers =>
      Behaviors.setup { context => {
        timers.startSingleTimer(UpdateData(List.empty), 0.second)
        timers.startTimerAtFixedRate(UpdateData(List.empty), 30.second)
        EventSourcedBehavior[CommandProtocol, EventProtocol, List[News]](
          persistenceId = PersistenceId.ofUniqueId("newsFeed"),
          emptyState = List.empty,
          commandHandler = commandHandler(bla, context),
          eventHandler = eventHandler)
          .withRetention(RetentionCriteria.snapshotEvery(numberOfEvents = 10, keepNSnapshots = 2)
            .withDeleteEventsOnSnapshot)
      }
      }
    }

  def getNews(bla: NaiveBayesLearningAlgorithm): Future[List[News]] = {
    val news: Future[Either[String, List[NewsFeed]]] = server.getNews
    news flatMap {
      case Left(_) => Future(List.empty)
      case Right(x) => {
        val result: List[Future[News]] = x.filter(_.text.nonEmpty).take(5).map(news => {
          val str = TextModifier.shielding(news.text)
          val bestClass = bla.classifier.classify(TextModifier.cleanAndConvert(str), 3)
          val sourse: Future[Option[SourceOfNews]] = if (news.sourceId > 0) {
            val user: Future[Either[String, List[User]]] = server.getUser(news.sourceId)
            user.map {
              case Left(_) => None
              case Right(x) => Some(x.map(z => SourceOfNews(z.firstName + " " + z.lastName,z.photo100)).head)
            }
          } else {
            val group = server.getGroups(Math.abs(news.sourceId))
            group.map {
              case Left(_) => None
              case Right(x) => Some(x.map(z => SourceOfNews(z.name, z.photo_100)).head)
            }
          }
          sourse.map(x => (News(TextMarking.markingTermsHTML(str, bestClass.terms), bestClass.category, x)))
        })
        Future.sequence(result)
      }
    }
  }
}