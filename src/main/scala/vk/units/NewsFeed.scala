package vk.units

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class NewsFeed(postId: Long, sourceId: Long, text: String, likesCount: Long)

object NewsFeed {
  implicit val newsFeedReads: Reads[NewsFeed] = (
    (__ \ "post_id").read[Long] and
      (__ \ "source_id").read[Long] and
      (__ \ "text").read[String] and
      (__ \ "likes" \ "count").read[Long]) (NewsFeed.apply _)
}

case class NewsFeedsResponse(newsFeed: List[NewsFeed])

object NewsFeedsResponse {
  implicit def newsFeedsReads: Reads[NewsFeedsResponse] = (__ \ "response" \ "items").read[List[NewsFeed]](Reads.list(NewsFeed.newsFeedReads)).map(NewsFeedsResponse(_))
}