package classifier

import classifier.NaiveBayesLearningAlgorithm.DataSet

/**
 * Обучающий алгоритм классификации
 */
case class NaiveBayesLearningAlgorithm() {

	private var examples: List[DataSet] = List()

	private val calculateWords: List[DataSet] => Int = (l: List[DataSet]) => l.map(_.examples.length).sum

	def getSizeExamples: Long = examples.size

	def addExample(ex: Vector[String], cl: String) {
		examples = DataSet(ex, cl.toLowerCase) :: examples
	}

	def addExamples(exs: List[DataSet]) {
		examples = exs.map(x => DataSet(x.examples, x.category.toLowerCase)) ++ examples
	}

	def dictionary: Set[String] = examples.flatMap(_.examples).toSet

	def model: NaiveBayesModel = {
		val docsByClass: Map[String, List[DataSet]] = examples.groupBy(_.category)
		val lengths: Map[String, Int] = docsByClass.map(x => (x._1, calculateWords(x._2)))
		val docCounts: Map[String, Int] = docsByClass.map(x => (x._1, x._2.length))
		val wordsCount: Map[String, Map[String, Int]] = docsByClass
			.map(x => (x._1, x._2.flatMap(_.examples).groupBy(y => y).map(z => (z._1, z._2.length))))

		NaiveBayesModel(lengths, docCounts, wordsCount, dictionary.size)
	}

	def classifier: NaiveBayesClassifier = NaiveBayesClassifier(model)
}

object NaiveBayesLearningAlgorithm{
	case class DataSet(examples: Vector[String], category: String)
}