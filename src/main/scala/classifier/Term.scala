package classifier

case class Term(word: String, start: Int, end: Int)
