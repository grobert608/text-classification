package classifier

case class ClassifyResult(category: String, terms: Seq[Term])
