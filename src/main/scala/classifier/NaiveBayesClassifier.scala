package classifier

import classifier.NaiveBayesClassifier.ProbabilityWithTops
import classifier.NaiveBayesModel.WordProbability

import scala.math.exp

/**
 * Алгоритм непосредственно классификации
 *
 * @param m статистическая модель классификатора
 */
case class NaiveBayesClassifier(m: NaiveBayesModel) {

  def classify(terms: Seq[Term], cnt: Int): ClassifyResult = {
    val res: (String, ProbabilityWithTops) = normalizer(m.classes.toList.map(c => (c, calculateProbability(c, terms, cnt)))).maxBy(_._2.probability)
    if (res._2.probability >= 0.7) ClassifyResult(res._1, res._2.topTerms)
    else ClassifyResult("neutral", Vector.empty)
  }

  /**
   * Рассчитывает оценку вероятности документа в пределах класса
   *
   * @param c класс
   * @param s текст документа
   * @return оценка <code>P(c|d)</code>
   */
  def calculateProbability(c: String, s: Seq[Term], cnt: Int): (ProbabilityWithTops) = {
    val wordLogProbabilityTerm: Seq[WordProbability] = s.map(m.wordLogProbability(c, _))
    ProbabilityWithTops(wordLogProbabilityTerm.map(_.probability).sum + m.classLogProbability(c), wordLogProbabilityTerm.sortBy(_.probability).map(_.term).distinctBy(_.word).takeRight(cnt))
  }

  def normalizer(res: Seq[(String, ProbabilityWithTops)]): Seq[(String, ProbabilityWithTops)] = {
    res.map(s => (s._1, ProbabilityWithTops(1 / res.map(x => exp(x._2.probability - s._2.probability)).sum, s._2.topTerms)))
  }
}

object NaiveBayesClassifier {

  case class ProbabilityWithTops(probability: Double, topTerms: Seq[Term])

}