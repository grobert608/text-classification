package textUtils

import java.nio.file.{Files, Path}

import com.opencsv._

import scala.collection.mutable.ListBuffer

object ReadDataFromCSV {
  def getExamples(path: Path): List[String] = {
    val builder = ListBuffer[String]()

    val bufferedReader = Files.newBufferedReader(path)

    val parser: CSVParser = new CSVParserBuilder()
      .withSeparator(';').build

    val reader: CSVReader = new CSVReaderBuilder(bufferedReader).withCSVParser(parser).build

    val csvIterator: CSVIterator = new CSVIterator(reader)

    while (csvIterator.hasNext) {
      csvIterator.next().lift(3).map(tweet => builder.addOne(tweet.trim.replace("\n", " ")))
    }

    reader.close
    bufferedReader.close
    builder.toList
  }
}
