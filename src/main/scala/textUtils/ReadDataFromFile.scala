package textUtils

import java.io.BufferedReader
import java.nio.file.{Files, Path}

import scala.collection.mutable.ListBuffer

object ReadDataFromFile {
  def getData(path: Path): List[String] = {

    val builder = ListBuffer[String]()

    val bufferedReader: BufferedReader = Files.newBufferedReader(path)

    while (bufferedReader.ready) {
      builder.addOne(bufferedReader.readLine.trim.replace("\n", " "))
    }

    bufferedReader.close
    builder.toList
  }
}