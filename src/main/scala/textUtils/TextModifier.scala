package textUtils

import classifier.Term
import org.apache.commons.text.StringEscapeUtils
import org.apache.lucene.analysis.ru.RussianAnalyzer
import org.apache.lucene.analysis.tokenattributes.{CharTermAttribute, OffsetAttribute}

import scala.collection.immutable.VectorBuilder

object TextModifier {
  def cleanAndSplit(string: String): Vector[String] = {
    val analyzer = new RussianAnalyzer()
    val ts = analyzer.tokenStream("text", string)
    ts.reset()
    val out = new VectorBuilder[String]
    while (ts.incrementToken()) {
      val word =
        ts.getAttribute(classOf[CharTermAttribute]).toString
      out += word
    }
    ts.close
    out.result
  }

  def cleanAndConvert(string: String): Vector[Term] = {
    val analyzer = new RussianAnalyzer()
    val ts = analyzer.tokenStream("text", string)
    ts.reset()
    val out = new VectorBuilder[Term]
    while (ts.incrementToken()) {
      val word =
        ts.getAttribute(classOf[CharTermAttribute]).toString
      val offsets = ts.getAttribute(classOf[OffsetAttribute])
      out += Term(word, offsets.startOffset(), offsets.endOffset())
    }
    ts.close
    out.result
  }

  def shielding(string: String): String = {
    StringEscapeUtils.escapeHtml4(string)
  }
}