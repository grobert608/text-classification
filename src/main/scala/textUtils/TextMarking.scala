package textUtils

import classifier.Term

object TextMarking {
  def markingTerms(string: String, terms: Seq[Term]): String = {
    val seq = terms.sortBy(_.start)
    val result = seq.indices.foldLeft(string)((z, x) => {
      z.patch(seq(x).start + 2 * x, "*", 0)
        .patch(seq(x).end + 2 * x + 1, "*", 0)
    })
    result
  }

  def markingTermsHTML(string: String, terms: Seq[Term]): String = {
    val seq = terms.sortBy(_.start)
    val left = "<span style=\"color: red\">"
    val right = "</span>"
    val leftLen = left.length
    val rightLen = right.length
    val result = seq.indices.foldLeft(string)((z, x) => {
      z.patch(seq(x).start + (leftLen + rightLen) * x, left, 0)
        .patch(seq(x).end + (leftLen + rightLen) * x + leftLen, right, 0)
    })
    result
  }
}
