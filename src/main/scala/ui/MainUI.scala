package ui

import javafx.application._
import javafx.scene.Scene
import javafx.scene.control.{Tab, TabPane}
import javafx.scene.layout.{Priority, VBox}
import javafx.stage.Stage
import ui.Classifier.ClassfierUI
import ui.LearningAlgorith.LearningAlgorithmUI

object MainUI {
  def main(args: Array[String]) {
    Application.launch(classOf[MainUI], args: _*)
  }
}

class MainUI extends Application {
  override def start(primaryStage: Stage) {
    primaryStage.setTitle("Text Classifier")

    val choiceAction = new TabPane

    val learn = new Tab("Learn Algorithm", LearningAlgorithmUI.root)
    learn.setClosable(false)

    val classify = new Tab("Classify Text", ClassfierUI.root)
    classify.setClosable(false)

    choiceAction.getTabs.add(learn)
    choiceAction.getTabs.add(classify)

    val vBox = new VBox(choiceAction)
    VBox.setVgrow(choiceAction, Priority.ALWAYS)

    primaryStage.setScene(new Scene(vBox, 700, 700))
    primaryStage.show
  }
}