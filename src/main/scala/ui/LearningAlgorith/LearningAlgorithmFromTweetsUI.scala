package ui.LearningAlgorith

import java.nio.file.Paths

import classifier.NaiveBayesLearningAlgorithm
import javafx.beans.binding.Bindings
import javafx.geometry.Insets
import javafx.scene.control.{Button, Label, TextField}
import javafx.scene.layout.{HBox, VBox}
import javafx.stage.{FileChooser, Stage}
import textUtils.{ReadDataFromCSV, TextModifier}

object LearningAlgorithmFromTweetsUI {
  val vboxRes = new VBox
  private val file = new FileChooser
  private val hboxClass = new HBox
  private val hboxPath = new HBox
  private val textFieldPath = new TextField
  private val textFieldClass = new TextField
  private val btnFile = new Button("Select File")
  private val btnLearn = new Button("Learn Algorithm")
  private val lablePath = new Label("Path:")
  private val lableClass = new Label("Enter Class:")

  file.getExtensionFilters.add(new FileChooser.ExtensionFilter("CSV", "*.csv"))

  hboxPath.getChildren.addAll(btnFile, lablePath, textFieldPath)
  hboxPath.setSpacing(10)

  hboxClass.getChildren.addAll(lableClass, textFieldClass)
  hboxClass.setSpacing(10)

  textFieldPath.setEditable(false)
  textFieldClass.setMinWidth(210)

  btnFile.setOnAction(_ => {
    textFieldPath.setText(file.showOpenDialog(new Stage()).getAbsolutePath)
  })

  btnLearn.disableProperty.bind(Bindings.isEmpty(textFieldPath.textProperty()).or(Bindings.isEmpty(textFieldClass.textProperty())))
  btnLearn.setOnAction(_ => {
    val ex = ReadDataFromCSV.getExamples(Paths.get(textFieldPath.getText)).map(res => NaiveBayesLearningAlgorithm.DataSet(TextModifier.cleanAndSplit(res), textFieldClass.getText))
    LearningAlgorithmUI.bla.addExamples(ex)
  })

  vboxRes.setSpacing(10)
  vboxRes.setPadding(new Insets(15, 20, 10, 10))

  vboxRes.getChildren.addAll(hboxClass, hboxPath, btnLearn)
}