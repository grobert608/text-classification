package ui.LearningAlgorith

import java.nio.file.Paths

import javafx.beans.binding.BooleanBinding
import javafx.geometry.Insets
import javafx.scene.Node
import javafx.scene.control.{Button, Label, TextField}
import javafx.scene.layout.{HBox, Priority, VBox}
import javafx.stage.{FileChooser, Stage}
import org.fxmisc.flowless.{Virtualized, VirtualizedScrollPane}
import org.fxmisc.richtext.{CodeArea, LineNumberFactory}
import textUtils.{ReadDataFromFile, TextModifier}

object LearningAlgorithmFromTextUI {
  val hboxRes = new HBox
  private val vboxRes = new VBox
  private val textForClassifier = new CodeArea
  private val hboxPath = new HBox
  private val hboxClass = new HBox
  private val hboxText = new HBox
  private val btnFile = new Button("Select File")
  private val file = new FileChooser
  private val btnLearn = new Button("Learn Algorithm")
  private val lablePath = new Label("Path:")
  private val textFieldPath = new TextField
  private val lableClass = new Label("Enter Class:")
  private val lableText = new Label("Enter Text:")
  private val textFieldClass = new TextField

  file.getExtensionFilters.add(new FileChooser.ExtensionFilter("TXT", "*.txt"))

  textFieldPath.setEditable(false)
  textFieldClass.setMinWidth(210)

  hboxPath.getChildren.addAll(btnFile, lablePath, textFieldPath)
  hboxPath.setSpacing(10)

  hboxClass.getChildren.addAll(lableClass, textFieldClass)
  hboxClass.setSpacing(10)

  textForClassifier.setParagraphGraphicFactory(LineNumberFactory.get(textForClassifier))
  private val virtualizedScrollPane = new VirtualizedScrollPane[Node with Virtualized](textForClassifier)

  hboxText.getChildren.addAll(lableText, virtualizedScrollPane)
  hboxText.setSpacing(10)

  vboxRes.setSpacing(10)
  vboxRes.getChildren.addAll(hboxClass, hboxPath, btnLearn)

  HBox.setHgrow(virtualizedScrollPane, Priority.ALWAYS)
  HBox.setHgrow(hboxText, Priority.ALWAYS)

  btnFile.setOnAction(_ => {
    val path = file.showOpenDialog(new Stage()).getAbsolutePath
    textFieldPath.setText(path)
    val data = ReadDataFromFile.getData(Paths.get(path)).mkString("\n")
    textForClassifier.appendText(data)
  })

  btnLearn.disableProperty.bind(new BooleanBinding {
    bind(textForClassifier.textProperty())
    bind(textFieldClass.textProperty())

    override def computeValue(): Boolean = textForClassifier.textProperty().getValue.isEmpty || textFieldClass.textProperty().getValue.isEmpty
  })

  btnLearn.setOnAction(_ => {
    LearningAlgorithmUI.bla.addExample(TextModifier.cleanAndSplit(textForClassifier.getText), textFieldClass.getText)
  })

  hboxRes.getChildren.addAll(hboxText, vboxRes)
  hboxRes.setSpacing(10)
  hboxRes.setPadding(new Insets(15, 20, 10, 10))
}