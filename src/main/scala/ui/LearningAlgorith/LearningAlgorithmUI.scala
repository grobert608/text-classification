package ui.LearningAlgorith

import classifier.NaiveBayesLearningAlgorithm
import javafx.collections.FXCollections
import javafx.geometry.Insets
import javafx.scene.control.{ComboBox, Label}
import javafx.scene.layout.{HBox, Priority, StackPane, VBox}

object LearningAlgorithmUI {
  val bla = NaiveBayesLearningAlgorithm()

  val root = new StackPane
  val hboxForComboBox = new HBox
  val vboxRoot = new VBox
  val vboxWithChoises = new VBox

  hboxForComboBox.setSpacing(10)
  hboxForComboBox.setPadding(new Insets(15, 20, 10, 10))

  val choiceList = FXCollections.observableArrayList("Learn Algorithm by Text or File", "Learn Algorithm by Tweets from File")
  val comboBox = new ComboBox[String](choiceList)
  comboBox.setValue("Learn Algorithm by Text or File")

  val lable = new Label("Select:")

  hboxForComboBox.getChildren.addAll(lable, comboBox)

  vboxRoot.getChildren.addAll(hboxForComboBox)

  VBox.setVgrow(vboxWithChoises, Priority.ALWAYS)
  VBox.setVgrow(LearningAlgorithmFromTextUI.hboxRes, Priority.ALWAYS)
  VBox.setVgrow(LearningAlgorithmFromTweetsUI.vboxRes, Priority.ALWAYS)

  vboxWithChoises.getChildren.add(LearningAlgorithmFromTextUI.hboxRes)

  vboxRoot.getChildren.add(vboxWithChoises)

  comboBox.setOnAction(_ => {
    comboBox.getValue match {
      case "Learn Algorithm by Tweets from File" => {
        vboxWithChoises.getChildren.removeAll(LearningAlgorithmFromTextUI.hboxRes)
        vboxWithChoises.getChildren.add(LearningAlgorithmFromTweetsUI.vboxRes)
      }
      case "Learn Algorithm by Text or File" => {
        vboxWithChoises.getChildren.removeAll(LearningAlgorithmFromTweetsUI.vboxRes)
        vboxWithChoises.getChildren.add(LearningAlgorithmFromTextUI.hboxRes)
      }
    }
  })

  root.getChildren.addAll(vboxRoot)
}