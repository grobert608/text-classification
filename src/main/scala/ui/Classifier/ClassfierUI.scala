package ui.Classifier

import javafx.beans.binding.BooleanBinding
import javafx.geometry.Insets
import javafx.scene.Node
import javafx.scene.control.{Button, TextField}
import javafx.scene.layout.{HBox, Priority, StackPane, VBox}
import org.fxmisc.flowless.{Virtualized, VirtualizedScrollPane}
import org.fxmisc.richtext.{CodeArea, LineNumberFactory}
import textUtils.{TextMarking, TextModifier}
import ui.LearningAlgorith.LearningAlgorithmUI

object ClassfierUI {

  val btnClassify = new Button
  val root = new StackPane
  val hbox = new HBox
  val vboxForResult = new VBox
  val textForClassifier = new CodeArea
  val resultOfClassifier = new TextField

  resultOfClassifier.setEditable(false)

  textForClassifier.setMinSize(300, 300)
  textForClassifier.setParagraphGraphicFactory(LineNumberFactory.get(textForClassifier))
  val virtualizedScrollPane = new VirtualizedScrollPane[Node with Virtualized](textForClassifier)

  hbox.setSpacing(10)
  hbox.setPadding(new Insets(15, 20, 10, 10))

  vboxForResult.setSpacing(10)
  vboxForResult.setPadding(new Insets(15, 20, 10, 10))

  btnClassify.disableProperty.bind(new BooleanBinding {
    bind(textForClassifier.textProperty())

    override def computeValue(): Boolean = textForClassifier.textProperty().getValue.isEmpty
  })

  btnClassify.setText("Classify")
  btnClassify.setOnAction(_ => {
    if (LearningAlgorithmUI.bla.getSizeExamples == 0) {
      resultOfClassifier.setText("Please learn Algorithm!")
    } else {
      val text: String = textForClassifier.getText
      val res = LearningAlgorithmUI.bla.classifier.classify(TextModifier.cleanAndConvert(text), 3)
      resultOfClassifier.setText(res.category)
      textForClassifier.replaceText(TextMarking.markingTerms(text, res.terms))
    }
  })

  vboxForResult.getChildren.addAll(btnClassify, resultOfClassifier)

  HBox.setHgrow(virtualizedScrollPane, Priority.ALWAYS)
  hbox.getChildren.addAll(virtualizedScrollPane, vboxForResult)

  root.getChildren.addAll(hbox)
}